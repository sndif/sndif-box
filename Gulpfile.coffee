gulp       = require 'gulp'
coffee     = require 'gulp-coffee'
concat     = require 'gulp-concat'
connect    = require 'gulp-connect'
less       = require 'gulp-less'
sass       = require 'gulp-sass'
minifyCSS  = require 'gulp-clean-css'
minifyHTML = require 'gulp-htmlmin'
minIMG     = require 'gulp-imagemin'
uglify     = require 'gulp-uglify'
watch      = require 'gulp-watch'
bowerMain  = require 'main-bower-files'
gulpFilter = require 'gulp-filter'
rename     = require 'gulp-rename'
flatten    = require 'gulp-flatten'
inject     = require 'gulp-inject'
pug        = require 'gulp-pug'
autoprefix = require 'gulp-autoprefixer'

config     = require './conf/gulp.json'
server     = require './conf/server.json'

gulp.task 'libraries', ->

  jsFilter    = gulpFilter [ '*.js', '**/*.js' ], { restore : true }
  cssFilter   = gulpFilter [ '*.css', '**/*.css' ], { restore : true }
  lessFilter  = gulpFilter [ '*.less', '**/*.less' ], { restore : true }
  sassFilter  = gulpFilter [ '*.sass', '**/*.sass', '*.scss', '**/*.scss' ], { restore : true }
  fontFilter  = gulpFilter ['*.eot', '**/*.eot', '*.woff', '**/*.woff', '*.woff2', '**/*.woff2', '*.svg', '**/*.svg', '*.ttf', '**/*.ttf'], { restore : true }
  imageFilter = gulpFilter ['*.gif', '**/*.gif', '*.png', '**/*.png', '*.svg', '**/*.svg', '*.jpg', '**/*.jpg', '*.jpeg', '**/*.jpeg'], { restore : true }

  return gulp
    .src bowerMain()
    # JS
    .pipe jsFilter
    .pipe uglify()
    .pipe rename( { suffix: ".min" } )
    .pipe gulp.dest('dev/lib/js')
    .pipe jsFilter.restore
    # CSS
    .pipe cssFilter
    .pipe autoprefix(
      browsers : [ 'last 5 versions' ]
      cascade  : false
    )
    .pipe minifyCSS( { compatibility: 'ie8' } )
    .pipe rename( { suffix: ".min" } )
    .pipe gulp.dest('dev/lib/css')
    .pipe cssFilter.restore
    # LESS
    .pipe lessFilter
    .pipe less().on('error', (err) ->
      console.log err.message
      @emit 'end'
    )
    .pipe autoprefix(
      browsers : [ 'last 5 versions' ]
      cascade  : false
    )
    .pipe minifyCSS( { compatibility: 'ie8' } )
    .pipe rename( { suffix: ".min" } )
    .pipe gulp.dest('dev/lib/css')
    .pipe lessFilter.restore
    # SASS
    .pipe sassFilter
    .pipe sass().on( 'error', sass.logError )
    .pipe autoprefix(
      browsers : [ 'last 5 versions' ]
      cascade  : false
    )
    .pipe minifyCSS( { compatibility: 'ie8' } )
    .pipe rename( { suffix: ".min" } )
    .pipe gulp.dest('dev/lib/css')
    .pipe sassFilter.restore
    # Images
    .pipe imageFilter
    .pipe flatten()
    .pipe gulp.dest('dev/lib/images')
    .pipe imageFilter.restore
    # Fonts
    .pipe fontFilter
    .pipe flatten()
    .pipe gulp.dest('dev/lib/fonts')
    .pipe fontFilter.restore

# Inject
gulp.task 'inject', [ 'libraries' ], ->

  conf = config.index

  target = gulp.src conf.source
  sources = gulp.src [
    'dev/lib/js/jquery.min.js'
    'dev/lib/js/angular.min.js'
    'dev/lib/js/angular-ui-router.min.js'
    'dev/lib/js/intro.min.js'
    'dev/lib/js/*.js'
    'dev/lib/css/*.css'
  ], { read: false }

  target
    .pipe inject( sources, { ignorePath: 'dev', addRootSlash: false } )
    # .pipe minifyHTML( { collapseWhitespace: true } )
    .pipe gulp.dest( conf.dest )

# Compile CoffeeScript
gulp.task 'coffee', ->
  conf = config.coffee
  gulp
    .src conf.source
    .pipe coffee().on( 'error', ( err ) ->
      console.log err.message
      @emit 'end'
    )
    #.pipe uglify()
    .pipe concat( conf.file )
    .pipe gulp.dest( conf.dest )
  return

# Compile Styles
gulp.task 'styles', ->
  conf = config.styles
  gulp
    .src conf.source
    .pipe sass().on( 'error', sass.logError )
    .pipe autoprefix(
      browsers : [ 'last 5 versions' ]
      cascade  : false
    )
    .pipe minifyCSS( compatibility: 'ie8' )
    .pipe concat( conf.file )
    .pipe gulp.dest( conf.dest )
  return

# Minify views
gulp.task 'views', ->
  conf = config.views
  gulp
    .src conf.source
    .pipe pug().on 'error', (err) ->
      console.log err
    # .pipe minifyHTML( { collapseWhitespace : true } )
    .pipe gulp.dest( conf.dest )
  return

# copy assets
gulp.task 'assets', ->
  conf = config.assets
  gulp
    .src conf.source
    .pipe gulp.dest conf.dest
  return

gulp.task 'compile', [
  'inject'
  'coffee'
  'styles'
  'views'
  'assets'
]

# Server
gulp.task 'serve', ->
  connect.server
    root       : server.root
    port       : server.port
    livereload : true
  return

# Reload
gulp.task 'reload', ->
  gulp
    .src config.dev
    .pipe connect.reload()
  return

gulp.task 'watch', ->
  gulp.watch [ config.coffee.source ], [ 'coffee' ]
  gulp.watch [ 'src/styles/*.sass', 'src/styles/**/*.sass' ], [ 'styles' ]
  gulp.watch [ config.views.source ], [ 'views' ]
  gulp.watch [ config.index.source ], [ 'inject' ]
  gulp.watch [ config.assets.source ], [ 'assets' ]
  gulp.watch [ config.dev ], [ 'reload' ] # livereload

gulp.task 'dev', [
  'compile'
  'serve'
  'watch'
]

###
# production
###
# Compile Styles
gulp.task 'styles:prod', ->
  conf = config.styles
  gulp
    .src conf.source
    .pipe sass().on( 'error', sass.logError )
    .pipe autoprefix(
      browsers : [ 'last 5 versions' ]
      cascade  : false
    )
    .pipe minifyCSS( compatibility: 'ie8' )
    .pipe concat( 'sndif.css' )
    .pipe gulp.dest 'dist'
  return

# copy assets
gulp.task 'assets:prod', ->
  conf = config.assets
  gulp
    .src conf.source
    .pipe gulp.dest 'dist/assets'
  return

gulp.task 'prod', [
  'styles:prod'
  'assets:prod'
]

gulp.task 'default', [ 'compile' ]

