angular
  .module 'app'
  .config [
    '$stateProvider'
    ( $stateProvider ) ->

      $stateProvider
        .state 'inside.list',
          url : 'list'
          templateUrl : 'views/states/inside/_views/_list.html'

  ]

